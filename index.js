const express = require ("express");
const mongoose = require ("mongoose");
const jquery = require ("jquery");
const routes = require("./routes");
const routesform = require('./routesform');
const bodyParser = require('body-parser');
const path = require('path');

const app = express();

//settings
app.set("appName", "cateringtrigana");
app.set("views", __dirname + "/views");
app.set("view engine", "ejs");
app.use(express.static(__dirname + '/public/css'));
app.use(express.static(__dirname + '/public/js'));
app.use(express.static(__dirname + '/controllers'));
app.use(express.static(__dirname + '/public/imagenes'));
app.use(express.static(path.resolve(__dirname, 'public')));

// DB
mongoose.connect("mongodb://aquarelladb:aquarella123456@ds111623.mlab.com:11623/aquarella", function (err) {
 if (err) throw err;
 console.log("Conectados a la base de datos");
 });

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// rutas
app.use("/", routes);

//rutas Formulario
app.use("/", routesform);

//Error 404
app.use(function(req, res){
    res.type('text/plain');
    res.status(404);
    res.send('404 - No encontrada');
});

// Error 500
app.use(function(err, req, res, next){
    console.error(err.stack);
    res.type('text/plain');
    res.status(500);
    res.send('500 - Server Error');
});

//localhost:1000
app.listen(process.env.PORT || 3000, function(){
    console.log("Express server listening on port %d in %s mode", this.address().port, app.settings.env);
});
