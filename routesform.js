const express = require ("express");
const router = express.Router();
const mongoose = require("mongoose");
const nodemailer = require('nodemailer');
const validator = require('express-validator');
const path = require('path');

const app = express();

//mailing puertos
var transporter = nodemailer.createTransport({
 service: 'gmail',
 auth: {
        user: 'christian@cateringtrigana.com',
        pass: 'gracias1000'
    }
});
//Schemas
var ReservasSchema = new mongoose.Schema({
  Nombre: String,
  Comensales: Number,
  Telefono: String,
  Email: String,
  Comentarios: String
});

const reservaModel = mongoose.model("reserva", ReservasSchema);

router.post("/form", (req, res) => {
    const formulario = new reservaModel(req.body);
    formulario.save(function (err, user) {
        if (err) {
          console.log(err);
          res.status(500).json({status: err})
        }
        else {
            const mailOptions = {
                from: 'christian@cateringtrigana.com',
                to: 'albertoseor@gmail.com',
                subject: 'Nueva reserva',
                html: `
                    <h1>Hola</h1>, <br> han hecho una reserva: <br>
                     Nombre: ${req.body.name},
                     Comensales: ${req.body.comensales},
                     Telefono: ${req.body.telefono},
                     Email: ${req.body.email},
                     Comentarios: ${req.body.comentarios},
                `
            };
            transporter.sendMail(mailOptions, function(error, info){
                if (error) {
                    console.log(error);
                } else {
                    console.log('Email sent: ' + info.response);
                }
            });
          res.status(200).json({status: 'ok'})
        }
      });
});


module.exports = router
