$(document).ready(function() {

    $('#form-reservas').validate({
      debug: true,

      submitHandler: function() {
          console.log('submitHandler')
          const Nombre = $('input[name="name"]').val();
          const Telefono = $('input[name="telefono"]').val();
          const Comensales = $('input[name="comensales"]').val();
          const Comentarios = $('textarea[name="comentarios"]').val();
          const Email = $('input[name="email"]').val();

          $.ajax({
              type: "POST",
              url: '/form',
              data: {
                  Nombre,
                  Comensales,
                  Telefono,
                  Email,
                  Comentarios
              },
              success: function() {
                  M.toast({
                      html: 'Reserva Enviada Con éxito! :) ',
                      classes: 'toast-form-success',
                  });
              },
              error: function( jqXHR, textStatus, errorThrown ) {
                  M.toast({
                      html: 'ERROR: No se ha efectuado la reserva! :( ',
                      classes: 'toast-form-error',
                  });
              }
          });
      },
      rules: {
          name: {
              required: true,
              minlength: 2
          },
          comensales: {
              required: true,
              number: true,
              range: [1, 25]
          },
          email: {
              required: true,
              email: true
          },
          telefono: {
              required: true,
              phone:true
          },
          comentarios: {
              maxlength: 1000
          },
      },
      errorLabelContainer: '.errorTxt'
  })
});

$.validator.addMethod( "phone", function( phone_number ) {
    return phone_number.match(/^(\+34|0034|34)?[6|7|9][0-9]{8}$/)
}, "" );
