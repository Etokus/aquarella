const express = require ("express");
const router = express.Router();

router.get("/", (req, res) => {
  res.render("index.ejs");
});

/* router.get("/eventos", (req, res) => {
  res.render("evento.ejs");
}); */

router.get("/gastronomia", (req, res) => {
  res.render("gastronomia.ejs");
});

router.get("/quien-somos", (req, res) => {
  res.render("quien-somos.ejs");
});

router.get("/cookies", (req, res) => {
  res.render("politica-cookie.ejs");
});

router.get("/privacidad", (req, res) => {
  res.render("politica-privacidad.ejs");
});

router.get("/aviso-legal", (req, res) => {
  res.render("aviso-legal.ejs");
});

router.get("*", function (req, res) {
  res.render("no-encontrado.ejs");
})

module.exports = router
